package com.android.test.uiautomator.demos;

import java.util.Arrays;

import org.json.JSONException;
import org.json.JSONObject;

import android.view.accessibility.AccessibilityNodeInfo;

import com.android.uiautomator.core.UiDevice;
import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiSelector;

public class CmdHandler {
	private String sCmd="";
	private final String[] singlecmds = new String[]{"home","enter","menu","delete","back","exit"};
	
	
	private static CmdHandler mcmdhandler;
	
	private CmdHandler(){
		
	}
	
	public static CmdHandler getInstance() {
	    if (null == mcmdhandler) {
	    	mcmdhandler = new CmdHandler();
	    }
	    return mcmdhandler;
	}
	
	/*
	 *{"action":"touch[down|up|move]/key[down|up]/type/tap/press/home/enter/menu",
	 *"key":"20", "x":"", "y":""} 
	 * */
	public Boolean parserCmd(String s, StringBuffer sb) 
	{
		sCmd = s;
		Boolean bRet = false;
		
		//Pattern p = Pattern.compile("(.*)(<div class=\"listren\">)(.*?)(</div>)(.*)");  
		//Matcher m = p.matcher(sCmd);  
		try {
			JSONObject cmdJson = new JSONObject(sCmd);
			String act = cmdJson.getString("action");
			if(Arrays.asList(singlecmds).contains(act)){
				doCmdSingle(act);
				return true;
			}

			int x = 0; 
			int y = 0;
			try{
				x = cmdJson.getInt("x");
				y = cmdJson.getInt("y");
			}catch(JSONException e)
			{
				e.printStackTrace();
				System.out.println("x, y, is error!");
			}
			
//			if("type".compareToIgnoreCase(act) == 0){
//				String sType = "";
//				try{
//					sType = cmdJson.getString("stype");
//				}catch(JSONException e)
//				{
//					e.printStackTrace();
//					System.out.println("stype is not found!");
//				}
//				
//				AccessibilityNodeInfo info = GetControlInfo(x, y);
//				if(info != null){
//					sb.append(info.toString());
//				}
//				UiObject uiobj = new UiObject(new UiSelector().className((String) info.getClassName())
//						.description((String) info.getContentDescription())
//						.checked(info.isChecked())
//						.enabled(info.isEnabled())
//						.text((String)info.getText()));
//				if(uiobj.exists()){
//					try {
//						uiobj.setText(sType);
//						bRet = true;
//					} catch (UiObjectNotFoundException e) {
//						e.printStackTrace();
//					}
//				}
//				
//				return bRet;
//			}
			if("click".compareToIgnoreCase(act)== 0){
				AccessibilityNodeInfo info = GetControlInfo(x, y);
				if(info != null){
					sb.append(info.toString());
					System.out.println(sb.toString());
				}
				
				UiDevice.getInstance().click(x, y);
				
			}else if("getbyxy".compareToIgnoreCase(act)==0){
				AccessibilityNodeInfo info = GetControlInfo(x, y);
				if(info != null){
					sb.append(info.toString());
					System.out.println(sb.toString());
				}
				
			}else if ("key".compareToIgnoreCase(act)==0){
				int keyCode = 0;
				UiDevice.getInstance().pressKeyCode(keyCode);
				
			}else if("typestring".compareToIgnoreCase(act)==0){
				String value="";
				MyUiObject uiobject = new MyUiObject(new UiSelector().focusable(true).focused(true));
				AccessibilityNodeInfo info = uiobject.findControlObject();
				if(info != null){
					sb.append(info.toString());
					System.out.println(sb.toString());					
				}
				try{
					value = cmdJson.getString("value");
				}catch (JSONException e) {
					e.printStackTrace();
				}
				try {
					if(uiobject.exists()){
						uiobject.setText(value);
					}
				} catch (UiObjectNotFoundException e) {
					e.printStackTrace();
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return bRet;
	}
	
	private AccessibilityNodeInfo GetControlInfo(int x, int y){
		MyUiObject myobj = new MyUiObject(new UiSelector());
        AccessibilityNodeInfo root = myobj.getRootObject();
        //if(root != null) System.out.println(root.toString());
        System.out.println(x);
        System.out.println(y);
        AccessibilityNodeInfo node = myobj.getControlObjectByXY(root, x, y);
        //if(node!=null) System.out.println(node.toString());
        
        return node;
	}
	
	
	private void doCmdSingle(String sC)
	{
		if("home".compareToIgnoreCase(sC)==0){
			UiDevice.getInstance().pressHome();
		}else if ("enter".compareToIgnoreCase(sC)==0){
			UiDevice.getInstance().pressEnter();
		}else if ("menu".compareToIgnoreCase(sC)==0){
			UiDevice.getInstance().pressMenu();
		}else if ("delete".compareToIgnoreCase(sC)==0){
			UiDevice.getInstance().pressDelete();
		}else if ("back".compareToIgnoreCase(sC)==0){
			UiDevice.getInstance().pressBack();
		}else if ("exit".compareToIgnoreCase(sC)==0){
			CmdServer.getInstance().CloseSever();
		}
		
	}
}
