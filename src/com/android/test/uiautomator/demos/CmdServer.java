package com.android.test.uiautomator.demos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

import android.text.TextUtils;

public class CmdServer {
	private ServerSocket serverSocket;
	Thread serverThread = null;
	public static final int SERVERPORT = 6000;
	
	private static CmdServer cmdserver =null;
	private byte[] waitobject = new byte[0];
	
	public static CmdServer getInstance()
	{
		if(cmdserver==null)
		{
			cmdserver = new CmdServer();
		}
		
		return cmdserver;
	}
	
	private CmdServer()
	{
		this.serverThread = new Thread(new ServerThread());
		this.serverThread.start();

	}

	public void waitThread()
	{
		try {
			synchronized(waitobject){
				waitobject.wait();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void notifyThread()
	{
		waitobject.notify();		
	}
	
	
	
	public void CloseSever() {
		try {
			serverThread.interrupt();
			serverSocket.close();
			notifyThread();
			cmdserver = null;
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	class ServerThread implements Runnable {

		public void run() {
	         Socket socket = null;

	         try {
	             serverSocket = new ServerSocket(SERVERPORT);
	         } catch (IOException e) {
	             e.printStackTrace();
	         }

	         while (!Thread.currentThread().isInterrupted()) {

	             try {
	                 socket = serverSocket.accept();
	                 CommunicationThread commThread = new CommunicationThread(socket);
	                 new Thread(commThread).start();
	             } catch (IOException e) {
	                 e.printStackTrace();
	             }
	         }
	     }
	 }


	 class CommunicationThread implements Runnable {

	     private Socket clientSocket;
	     private BufferedReader input;
	     private BufferedWriter output;
	     public CommunicationThread(Socket clientSocket) {
	         
	    	 this.clientSocket = clientSocket;

	         try {
	             this.input = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
	             this.output = new BufferedWriter(new OutputStreamWriter(this.clientSocket.getOutputStream()));
	         } catch (IOException e) {
	             e.printStackTrace();
	         }
	     }

	     public void Senddata(String str){
	    	 if(!TextUtils.isEmpty(str))
	    	 {
	    		 try {
	    			System.out.print("Send data:");
	    			System.out.print(str);
					output.write(str+"\r\n");
					output.flush();
				} catch (IOException e) {
					e.printStackTrace();
				}
	    	 }
	     }
	     

	     public void run() {
	    	 StringBuffer sb = new StringBuffer();
	         while (!Thread.currentThread().isInterrupted()) {
	             try {
	                 String read = input.readLine();
	                 if(read != null)
	                 {
	                	 System.out.println(read);
	                	 CmdHandler.getInstance().parserCmd(read, sb);
	                	 Senddata(sb.toString());
	                	 sb.setLength(0);
	                 }
	             } catch (IOException e) {
	                 e.printStackTrace();
	                 Thread.currentThread().interrupt();
	             }
	         }
	     }
	 }
	
}
