/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.android.test.uiautomator.demos;

import android.os.RemoteException;
import android.util.Log;
import android.view.accessibility.AccessibilityNodeInfo;

import com.android.uiautomator.core.UiDevice;
import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiScrollable;
import com.android.uiautomator.core.UiSelector;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;

/**
 * This demos how we read content-description to properly open the
 * All Apps view and select and application to launch. Then we will
 * use the package name to verify that the current window is actually
 * from the expected package
 */
public class LaunchSettings extends UiAutomatorTestCase {
	public static final String LOG_TAG = LaunchSettings.class.getSimpleName();
	
    /**
     * For the purpose of this demo, we're declaring the Settings signatures here.
     * It may be more appropriate to declare signatures and methods related
     * to Settings in their own reusable Settings helper file.
     */
    public static class SettingsHelper {
        public static final UiSelector LIST_VIEW =
                new UiSelector().className(android.widget.ListView.class.getName());
        public static final UiSelector LIST_VIEW_ITEM =
                new UiSelector().className(android.widget.LinearLayout.class.getName());
    }
	
    public class ServerThread extends Thread{
    	
		@Override
		public void run() {
			CmdServer.getInstance();
			super.run();
			
		}
    	
		public void stopserver(){
			CmdServer.getInstance().CloseSever();
		}
    }
    
    
    ServerThread  ssthrd;
    private void SocketSever()
    {
    	ssthrd = new ServerThread();
    	ssthrd.start();
    }
    
    public void testDemo() throws UiObjectNotFoundException {
        // Good practice to start from a common place
        try {
			getUiDevice().wakeUp();
			getUiDevice().setOrientationNatural();
			getUiDevice().freezeRotation();
			//getUiDevice().openQuickSettings();
			//UiDevice.getInstance().openQuickSettings();
		} catch (RemoteException e2) {
			e2.printStackTrace();
		}
        
    	getUiDevice().pressHome();
    	//UiObject allAppsButton = new UiObject(new UiSelector());

    	//SocketSever();
    	CmdServer.getInstance();
    	CmdServer.getInstance().waitThread();
    	System.out.println("Thread has run");
    	        
        //CmdServer.getInstance().CloseSever();
        
        return;
/*
        // When we use the uiautomatorviewer in the DSK/tools we find that this
        // button has the following content-description
        UiObject allAppsButton = new UiObject(new UiSelector().description("Apps"));
        
        MyUiObject myobj = new MyUiObject(null);
        AccessibilityNodeInfo root = myobj.getRootObject();
        AccessibilityNodeInfo node = myobj.getControlObjectByXY(root, 600, 1600);
        System.out.println(node.toString());

        // ** NOTE **
        // Any operation on a UiObject that is not currently present on the display
        // will result in a UiObjectNotFoundException to be thrown. If we want to
        // first check if the object exists we can use allAppsButton.exists() which
        // return boolean.
        // ** NOTE **

        //The operation below expects the click will result a new  window.
        allAppsButton.clickAndWaitForNewWindow();

        // On the this view, we expect two tabs, one for APPS and another for
        // WIDGETS. We will want to click the APPS just so we're sure apps and
        // not widgets is our current display
        UiObject appsTab = new UiObject(new UiSelector().text("Apps"));

        // ** NOTE **
        // The above operation assumes that there is only one text "Apps" in the
        // current view. If not, then the first "Apps" is selected. But if we
        // want to be certain that we click on the tab "Apps", we can use the
        // uiautomatorview from the SDK/tools and see if we can further narrow the
        // selector. Comment the line above and uncomment the one bellow to use
        // the more specific selector.
        // ** NOTE **

        // This creates a selector hierarchy where the first selector is for the
        // TabWidget and the second will search only inside the layout of TabWidget
        // To use this instead, uncomment the lines bellow and comment the above appsTab
        //UiSelector appsTabSelector =
        //        new UiSelector().className(android.widget.TabWidget.class.getName())
        //            .childSelector(new UiSelector().text("Apps"));
        //UiObject appsTab = new UiObject(appsTabSelector);


        // The operation below we only cause a content change so a click() is good
        appsTab.click();
*/
        // Since our device may have many apps on it spanning multiple views, we
        // may need to scroll to find our app. Here we use UiScrollable to help.
        // We declare the object with a selector to a scrollable view. Since in this
        // case the firt scrollable view happens to be the one containing our apps
        // list, we should be ok.
/*     
        for(int i = 0; i < 10; i++)
        {
	        try
	        {
		        UiSelector appDemoList = new UiSelector().className(android.widget.TextView.class.getName()).text("Settings");
		        Log.d(LOG_TAG, appDemoList.toString());
		        UiObject uisettings = new UiObject(appDemoList);
		        uisettings.clickAndWaitForNewWindow();
		        break;
	        }
	        catch(Exception e)
	        {
	        	UiDevice deviceInstance = UiDevice.getInstance();
	            int dHeight = deviceInstance.getDisplayHeight();
	            int dWidth = deviceInstance.getDisplayWidth();
	            System.out.println("height =" +dHeight);
	            System.out.println("width =" +dWidth);
	            int yScrollStop = dHeight/2;
	            UiDevice.getInstance().swipe(
	            	dWidth - 10, 
	            	yScrollStop, 
	                10, 
	                yScrollStop, 
	                100
	            );
	        }
        }
//*/
/*        
        UiScrollable appViews = new UiScrollable(new UiSelector().scrollable(true));
        // swipe horizontally when searching (default is vertical)
        appViews.setAsHorizontalList();
       

        // the appsViews will perform horizontal scrolls to find the Settings app
        appViews.scrollIntoView(new UiSelector().className(android.widget.TextView.class.getName()).text("Settings"));
        UiObject settingsApp = appViews.getChildByText(
               new UiSelector().className(android.widget.TextView.class.getName()), "Settings");
        settingsApp.clickAndWaitForNewWindow();
//*/
        // create a selector for anything on the display and check if the package name
        // is the expected one
/*
        UiObject settingsValidation =
                new UiObject(new UiSelector().packageName("com.android.settings"));

        assertTrue("Unable to detect Settings", settingsValidation.exists());
        
        UiScrollable appsSettingsList = new UiScrollable(SettingsHelper.LIST_VIEW);
        UiObject obj = appsSettingsList.getChildByText(SettingsHelper.LIST_VIEW_ITEM, "Bluetooth");
        Log.d(LOG_TAG, obj.getSelector().toString());
        obj.getChild(new UiSelector().className(android.widget.Switch.class.getName())).click();      
        
        if (!selectSettingsFor("About phone"))
            selectSettingsFor("About tablet");

        selectSettingsFor("Software information");
        // Now we need to read the Build number text and return it
        String buildNum = getAboutItem("Build number");

        // Log it - Use adb logcat to view the results
        Log.i(LOG_TAG, "Build = " + buildNum);
    */    
        
    }
    
    /**
     * Select a settings items and perform scroll if needed to find it.
     * @param name
     */
    private boolean selectSettingsFor(String name)  {
        try {
            UiScrollable appsSettingsList = new UiScrollable(SettingsHelper.LIST_VIEW);
            UiObject obj = appsSettingsList.getChildByText(SettingsHelper.LIST_VIEW_ITEM, name);
            Log.d(LOG_TAG, obj.getSelector().toString());
            obj.click();
        } catch (UiObjectNotFoundException e) {
            return false;
        }
        return true;
    }

    /**
     * This function will detect the presence of 2 or 1 list view display fragments and
     * targets the correct list view for the About item details
     * @param item
     * @return the details string of an about item entry
     * @throws UiObjectNotFoundException
     */
    private String getAboutItem(String item) throws UiObjectNotFoundException {
        // try accessing the second list view if one exists else we will assume the
        // device is displaying a single list view
        UiScrollable aboutSettingsList = new UiScrollable(SettingsHelper.LIST_VIEW.instance(1));
        if (!aboutSettingsList.exists())
            aboutSettingsList = new UiScrollable(SettingsHelper.LIST_VIEW.instance(0));

        // the returned aboutItem will be pointing at the node matching the
        // SettingsOsr.LIST_VIEW_ITEM. So, aboutItem is a container of widgets where one
        // actually contains the text (item) we're looking for.
        UiObject aboutItem = aboutSettingsList.getChildByText(SettingsHelper.LIST_VIEW_ITEM, item);

        // Since aboutItem contains the text widgets for the requested details, we're assuming
        // here that the param 'item' refers to the label and the second text is the value for it.
        UiObject txt = aboutItem.getChild(
                new UiSelector().className(android.widget.TextView.class.getName()).instance(1));

        // This is interesting. Since aboutItem is returned pointing a the layout containing the
        // test values, we know it is visible else an exception would've been thrown. However,
        // we're not certain that the instance(1) or second text view inside this layout is
        // in fact fully visible and not off the screen.
        if (!txt.exists())
            aboutSettingsList.scrollForward(); // scroll it into view

        return txt.getText();
    }
    
}
